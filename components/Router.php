<?php

class Router
{

    public function run()
    {
        $uri = $this->getURI();
        //TODO: понять как работает. Готово
        switch (1) {
            case preg_match('/^\/$/', $uri) :
                $this->getController()->taskList();
                break;
            case preg_match('/^\/login$/', $uri) :
                $this->getController()->login();
                break;
            case preg_match('/^\/task\/[0-9]*$/', $uri, $match) :
                $split = explode('/', $uri);
                $this->getController()->task($split[2]);
                break;
            case preg_match('~^/logout$~', $uri) :
                $this->getController()->logout();
                break;
            case preg_match('~^/update_task$~', $uri) :
                $this->getController()->updateTask();
                break;
            case preg_match('~^/editing/[0-9]*$~', $uri) :
                $split = explode('/', $uri);
                $this->getController()->editing($split[2]);
                break;
            default:
                //TODO: Сделать вью для 404той ошибки (страница не найдена). Готово
                $this->getController()->pageNotFound();
        }
    }

    //TODO: возможно избыточный метод
    private function getURI()
    {
        return $_SERVER['REQUEST_URI'];
    }

    private function getController()
    {
        include_once ROOT . '/controllers/controller.php';
        include_once ROOT . '/db/connection.php';
        include_once ROOT . '/model/model.php';
        include_once ROOT . '/view/view.php';
        $connection = new Connection();
        $model = new Model($connection);
        $view = new View($model);
        return new Controller($model, $view);
    }

}
