<?php

class Controller {

    private $model;
    private $view;

//TODO: работает, + показывает подсказки в моей ide(т.к. знает какой класс)
    function __construct(Model $model, View $view) {
        $this->model = $model;
        $this->view = $view;
    }

    function login() {
        $_SESSION['name'] = $_POST['login'];
        $login = $_POST['login'];
        $password = $_POST['password'];
        if ($login && $password) {
            $wrong = true;
            $this->isWrongAuthorization($wrong);
            if ($this->model->checkLoginPassword($login, $password)) {
                $this->view->getTasksListView();
            } else {
//TODO: отобразить ошибку на форме логина (через html) чтобы имя пользователя не стиралось при неудачной попытке. Готово
                $this->view->getLoginView();
                $wrong = false;
                $this->isWrongAuthorization($wrong);
            }
        } else {
            $this->view->getLoginView();
        }
    }

    function taskList() {
        $this->view->getTasksListView();
    }

//TODO: картинке в листе кликабельны теперь, понять как сделано
    function task($id) {
        $this->view->getTask($id);
    }

    function pageNotFound() {
        $this->view->getPageNotFound();
    }

    function isWrongAuthorization($wrong) {
        if (!$wrong) {
            return 'Не правильный логин или пароль';
        }
    }

    function editing($id) {
        $this->view->getTask($id);
    }

    function logout() {
        $this->view->getLogout();
    }
    function updateTask(){
        $this->model->updateTask();
        $this->view->getTask($_POST['id']);
    }

}
