<!DOCTYPE html>
<html
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group">
                        <?php if(($_SESSION['login'] == 0)): ?>
                        <a class="btn btn-default" type="button" href="/login">
                            Администратор
                        </a>
                        <?php else: ?>
                        <a class="btn btn-default" type="button" href="/logout">
                            Выйти
                        </a>
                        <?php endif; ?>
                        <button class="btn btn-default" type="button">
                            Добавить задачу
                        </button> 
                        <button class="btn btn-default" type="button">
                            Right
                        </button> 
                        <button class="btn btn-default" type="button">
                            Justify
                        </button>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><?php echo $res['text'];?>
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>