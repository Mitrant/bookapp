<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php require_once (ROOT . '/layouts/index.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <form method="post" role="form">
                <div class="form-group">

                    <label for="exampleInputEmail1">
                        Login
                    </label>
                    <input name="login" type="text" value="<?php echo $_SESSION['name']; ?>" class="form-control" id="exampleInputEmail1" />
                </div>
                <div class="form-group">

                    <label name="password" for="exampleInputPassword1">
                        Password
                    </label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" />
                </div>
                <div class="form-group">

                    <label for="exampleInputFile">
                        File input
                    </label>
                    <input type="file" id="exampleInputFile" />
                    <p class="help-block">
                        Example block-level help text here.
                    </p>
                </div>
                <div class="checkbox">

                    <label>
                        <input type="checkbox" /> Check me out
                    </label>
                </div> 
                <button type="submit" class="btn btn-default">
                    Submit
                </button>
            </form>
            <!--Начало блока с ошибкой-->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <h3 style="color: red;">
                            
                            <?php if (isset($_POST['login']) && $_SESSION['login'] == 0) {
                                echo 'Не верный логин или пароль';
                            }
                            ?>
                        </h3>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
            <!--Конец блока с ошибкой-->
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>