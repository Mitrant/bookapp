<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
                            <h1>
					Error 404! <p><small>Page is not found!</small></p>
				</h1>
			</div>
		</div>
	</div>
</div>