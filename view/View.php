<?php

class View
{

    private $model;

    function __construct(Model $model)
    {
        $this->model = $model;
    }

    function getLoginView()
    {
        include_once ROOT . '/layouts/login.php';
    }

    function getTasksListView()
    {
        $row = $this->model->getTasks();
        include_once ROOT . '/layouts/taskList.php';
    }

    function getTask($id)
    {
        $connection = $this->model;
        include_once ROOT . '/layouts/index.php';
        $row = $this->model->getTask($id);
        $res = $row->fetch_assoc();
        include_once ROOT . '/layouts/editing.php';        
    }
    
    function getPageNotFound(){
        include_once ROOT . '/layouts/pageNotFound.php';
    }
    
    function getLogout(){
        $_SESSION['login'] = false;
        $row = $this->getTasksListView();
        include_once ROOT . '/layouts/taskList.php';
    }

}
